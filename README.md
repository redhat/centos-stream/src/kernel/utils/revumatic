# What is Revumatic

Revumatic is a review tool for developers of Red Hat kernels. It provides an
interactive TUI (text user interface) to review GitLab merge requests.

# Features

- provides interactive text user interface
- understands Red Hat kernel specific MR metadata
- compares backports to upstream with highlighted differences
- fetches upstream commits that you don't have locally
- displays missing fixes
- can post your comments
- can approve and unapprove
- shows only relevant comments, hides the noise from CKI bot
- opens Bugzilla with a single key
- shows your GitLab TODO list
- needs no configuration

# Installation

For Fedora and RHEL, there is
[a yum/dnf repository](https://copr.fedorainfracloud.org/coprs/jbenc/review-tools/)
available.

On other distros or to help developing Revumatic itself, clone this repo.
Revumatic can be started directly from a checked out tree by `./revumatic`.
To install the required Python dependencies, run `pip3 install --user -r requirements.txt`.
Note that you'll likely need python-devel, gcc and other packages to
successfully run pip3.

# Usage

## General notes

Revumatic is designed to work outside of Red Hat VPN. However, if you're not
on Red Hat VPN, you won't get a list of missing fixes, since that feature
depends on the Kerneloscope server which is not available publicly.

## Starting Revumatic

Run `revumatic` without arguments and select the desired action from the
menu. The bottom line of the screen lists the most important keys that
you can use at any moment.

You can also start each action directly from the command line, see the
on-screen help for details, or run `revumatic --help`.

# FAQ, Tips and Tricks

## UI customization

You can change the UI layout, colors, customize key bindings, and more. Just
start Revumatic and configure it to your liking.

## Adding upstream repos

While Revumatic can fetch upstream commits online, it's not always practical.
Before it reaches out to online services, it first searches all configured
local repositories for the commit. Which means you can just add your local
clones of upstream trees to the Revumatic config.

Start Revumatic, open its configuration and look under __Common options__
/ __Local git repositories__.

## Quick way to open a MR

Start Revumatic with the merge request URL:

```
revumatic https://gitlab.com/[...]/-/merge_requests/834
```

## Quicker way to open a MR

When you are in the git repository holding your Red Hat kernel, start
Revumatic just with the merge request number:

```
cd ~/git/rhel-9
revumatic 834
```

## Can I have several RH kernels in the same local repo?

Yes. Revumatic will pick the right git remote automatically.

## Do missing fixes account for already backported fixes?

Yes. If a fix is identified but it was already applied earlier to the RHEL
kernel out of order, it will not be shown.

## Are my unsubmitted comments saved when I quit Revumatic?

Yes. Revumatic will not lose your comments. When you open the same MR, your
comments will still be there. If the MR gets changed and rebased meanwhile,
Revumatic will still preserve your comments.

## Some characters (usually emoji icons) are not rendered correctly

Your terminal likely does not support Unicode fullwidth characters. See the
__Formatting options__ in Revumatic config for possible workarounds.

## Can Revumatic be used for other packages than the kernel?

Revumatic is ready for that. It is modular and parsing of metadata of other
packages is just a matter of implementing a single class. Right now, only
the kernel parsing is implemented.

# License

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.
