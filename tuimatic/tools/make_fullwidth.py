import urllib.request

def escape(num):
    if num < 0x100:
        return '\\x{:02x}'.format(num)
    if num < 0x10000:
        return '\\u{:04x}'.format(num)
    return '\\U{:08x}'.format(num)

def process_east_asian_width(f):
    out = []
    last = None
    for line in f.readlines():
        line = line.decode()
        line = line.rstrip('\n')
        if line.startswith('#') or not line:
            continue
        hex, line = line.split(';', 1)
        wid, line = line.split(' # ', 1)
        cat, line = line.split(' ', 1)

        wid = wid.strip()
        cat = cat.strip()

        if '..' in hex:
            hex = hex.split('..')
        else:
            hex = (hex, hex)
        start = int(hex[0], 16)
        end = int(hex[1], 16)

        if (cat == 'Lm' # modifier letter
            or cat[0] == 'M' # combining characters
            or cat[0] == 'C' # other characters, such as control
           ):
            l = 0
        elif wid in ('W', 'F'):
            l = 2
        else:
            l = 1

        if last == l and out[-1][1] + 1 == start:
            out[-1][1] = end
        else:
            out.append([start, end, l])
        last = l

    return ['{}-{}'.format(escape(start), escape(end))
            for start, end, width in out
            if width == 2]

if __name__ == '__main__':
    f = urllib.request.urlopen('https://www.unicode.org/Public/UCD/latest/ucd/EastAsianWidth.txt')
    print(''.join(process_east_asian_width(f)))
