import re
import socket
import tuimatic

from modules.settings import settings
from modules.utils import lab
from modules import (
    connection,
    gitlab,
    kernel_owners,
    kerneloscope,
    mr_activity,
    packages,
)

DEFAULT_KERNELOSCOPE = 'kerneloscope.usersys.redhat.com'

class Kernel(packages.Package):
    paths = set((
        # the ARK repo:
        'cki-project/kernel-ark',
        # the CentOS Stream repos:
        'redhat/centos-stream/src/kernel/',
        # the AutoSD kernel repo:
        'CentOS/automotive/src/kernel/kernel-automotive-9',
        # the rest is fetched dynamically in dynamic_init
    ))
    hidden_paths = (
        '.*/rhel-[0-9]*-sandbox$',
    )
    marks = 1

    @classmethod
    def register_settings(cls):
        sect = settings.section('kernel', 'Kernel specific options', 8)
        sect.register('kerneloscope', str, default=DEFAULT_KERNELOSCOPE,
                      display_name='Kerneloscope server',
                      help_text='''The hostname of the Kerneloscope server. This config
                                   option will be ignored if 'Red Hat internal VPN' is set
                                   to 'no'.''')
        sect.register('subsystem', [str, ' '], default=[],
                      display_name='Subsystem list',
                      help_text='''The subsystem labels (without the "Subsystem:" part) that
                                   you are interested in. If specified, the TODO list and
                                   other lists will show missing approvals for those
                                   specified subsystems. If not specified, the generic
                                   Acks::NeedsReview labels will be shown instead. For
                                   example, it this is set to 'net', 'Acks::net::NeedsReview'
                                   labels will be shown in the lists.''')

    @classmethod
    def dynamic_init(cls):
        # check for paths
        for group in lab.group('redhat').descendant_groups(search='kernel'):
            path = group['full_path']
            if 'sst' in path or 'tools' in path or '/tests/' in path:
                continue
            # groups need to end with a slash
            cls.paths.add(path + '/')
        # check for Kerneloscope accessibility
        if 'rh_vpn' not in settings['global']:
            server = settings['kernel']['kerneloscope']
            try:
                socket.getaddrinfo(server, 80)
                # the Kerneloscope hostname is resolvable; assume RH VPN
                # access from now on, even on the next Revumatic runs
                settings['global']['rh_vpn'] = True
            except socket.gaierror:
                pass

    def __init__(self):
        self.kgit = connection.https_pool('git.kernel.org')
        self.kscope = None
        if settings['global']['rh_vpn']:
            server = settings['kernel']['kerneloscope']
            self.kscope = kerneloscope.Kerneloscope(server, error_notify=self.kscope_error)

        self.owners = None
        self.subsystems = [s.lower() for s in settings['kernel']['subsystem']]
        self.review_labels = []
        self.review_labels_fallback = []
        need_fallback = True
        for subs in self.subsystems or ('all',):
            if subs == 'all':
                self.review_labels.append('acks::needsreview')
                need_fallback = False
            else:
                self.review_labels.append('acks::{}::needsreview'.format(subs))
        if need_fallback:
            self.review_labels_fallback.append('acks::needsreview')

        self._bugzilla_re = re.compile(r'^(?i:bugzilla): (https?://bugzilla\.redhat\.com/[^ ]*) *$',
                                       re.MULTILINE)
        self._jira_re = re.compile(r'^(?i:jira): (https://issues\.redhat\.com/[^ ]*) *$',
                                       re.MULTILINE)
        self._upstream_re = re.compile(r'^commit ([0-9a-f]{40})( \(.*\))?$|^\(cherry picked from commit ([0-9a-f]{40})\)$',
                                       re.MULTILINE)
        self._rhel_only_re = re.compile(r'^upstream[ -]status:.*rhel[ _-]?[0-9]*([0-9]\.[0-9]+)?[ _-]only',
                                        re.MULTILINE | re.IGNORECASE)
        self._omit_re = re.compile(r'^(?i:omitted[ -]fix): ([0-9a-f]{8,40})( .*)?$',
                                   re.MULTILINE)
        self._kabi_re = re.compile('RH_KABI_|__GENKSYMS__')
        self._nack_re = re.compile(r'^ *Nacked-by: ', re.MULTILINE)

        self._cmd_re = re.compile(r'^(request-([a-z-]*-)?evaluation|/block|/unblock)$')

        self._bot_re = re.compile(r'^group_.*_bot')

    def kscope_error(self, error):
        tuimatic.emit_signal(self, 'status-changed')

    def get_status(self):
        return ('header-problem', 'K✘') if self.kscope and self.kscope.error else ''

    def get_extra_help(self):
        if self.kscope and self.kscope.error:
            return [('title', 'Error status:\n\n'),
                    ('list-error', 'K✘'), '  Kerneloscope: {}\n'.format(self.kscope.error)
                   ]
        return None

    def is_hidden(self, path):
        for hidden in self.hidden_paths:
            if re.match(hidden, path):
                return True
        return False

    def parse_dep_label(self, label):
        dep = label.split('::')[-1]
        if dep == 'OK':
            return None
        return dep

    def get_base_sha(self, mr):
        for label in mr['labels']:
            if label.startswith('Dependencies::'):
                return self.parse_dep_label(label)
        return None

    def get_historical_base_sha(self, version, activity):
        deps = None
        for item in activity.data:
            if (isinstance(item, mr_activity.VersionActivityItem) and
                item.version_number > version):
                break
            if (isinstance(item, mr_activity.LabelActivityItem) and
                item.label_data['action'] == 'add' and
                item.label_data['label'] and
                item.label_data['label']['name'].startswith('Dependencies::')):
                deps = self.parse_dep_label(item.label_data['label']['name'])
        return deps

    def get_bug_urls(self, mr):
        result = [match.group(1).rstrip()
                  for match in self._bugzilla_re.finditer(mr['description'])]
        result.extend(match.group(1).rstrip()
                      for match in self._jira_re.finditer(mr['description']))
        return result

    def get_upstream(self, commit):
        matches = []
        for match in self._upstream_re.finditer(commit.message):
            if match.group(1) is None:
                matches.append(match.group(3))
            else:
                matches.append(match.group(1))
        if matches:
            return matches
        match = self._rhel_only_re.search(commit.message)
        if match is None:
            return []
        return None

    def get_kabi_mark(self, diff):
        for f in diff:
            for h in f:
                if (self._kabi_re.search(h.lines_added()) or
                    self._kabi_re.search(h.lines_removed())):
                    return True
        return False

    def get_marks(self, commit, diff):
        kabi_mark = 'K' if self.get_kabi_mark(diff) else ' '
        return (kabi_mark,)

    def get_marks_help(self):
        return (('K', 'kABI modifications', 'err'),)

    def get_label_color(self, label):
        cls = ''
        if label in ('Bugzilla::Missing', 'Bugzilla::Closed', 'JIRA::Missing', 'JIRA::Closed',
                     'CommitRefs::Missing', 'Signoff::NeedsReview', 'Merge::Conflicts'):
            return 'err'
        if label in ('readyForMerge', 'readyForQA', 'approved'):
            return 'ok'
        components = label.split('::')
        if (components[0] == 'Dependencies' and len(components) == 2 and
            len(components[1]) == 12):
            # Dependencies::hash
            return 'warn'
        for c in components[1:]:
            if c == 'OK':
                cls = 'ok'
            elif c.startswith('Needs') or c == 'Warning':
                cls = 'warn'
            elif (c.startswith('NACK') or c.endswith('Failed') or c.endswith('Blocked') or
                  c == 'Invalid'):
                cls = 'err'
        if components[0].startswith('CKI_') and cls == 'err':
            cls = 'warn'
        return cls

    def filter_list_labels(self, labels, flags):
        review_negative = []
        review_of_interest = []
        review_global = []
        always_of_interest = []
        other_blockers = []
        approved = False
        for label in labels:
            lower_label = label.lower()
            if lower_label in self.review_labels:
                review_of_interest.append(label)
            elif lower_label in self.review_labels_fallback:
                review_global.append(label)
            elif lower_label in ('acks::nacked', 'acks::blocked'):
                review_negative.append(label)
            elif not flags:
                if lower_label == 'acks::ok':
                    approved = True
                elif (lower_label in ('readyformerge', 'readyforqa', 'lnst::failed',
                                      'externalci::failed', 'merge::conflicts') or
                      lower_label.startswith('cki::failed')):
                    always_of_interest.append(label)
                elif (lower_label == 'lnst::needstesting' or
                      lower_label == 'externalci::needstesting' or
                      lower_label.endswith('::needsreview')):
                    other_blockers.append(label)
                elif (lower_label in ('bugzilla::missing', 'bugzilla::closed',
                                      'jira::missing', 'jira::closed', 'merge::warning')):
                    other_blockers.append(label)
        result = review_negative or review_of_interest or review_global
        if result:
            # the MR is waiting for review, do not show other blockers
            other_blockers = []
        elif approved and not always_of_interest:
            result.append('approved')
        result.extend(always_of_interest)
        result.extend(other_blockers)
        return result

    def get_todo_action(self, raw_data):
        if (raw_data['action_name'] == 'mentioned' and
            raw_data['author']['username'] == 'cki-kwf-bot' and
            'Requesting review of subsystem' in raw_data['body']):
            return { 'action': 'review of' }
        return None

    def get_blocks(self, mr):
        result = []
        # mr.approval_state is already cached, this will not cause an API call
        for rule in mr.approval_state['rules']:
            if rule['name'].startswith('Blocked-by:') and not rule['approved']:
                result.extend(rule['eligible_approvers'])
        return result

    def block(self, mr):
        # safeguard against concurrent access from web and Revumatic: check
        # existence of the rule on the server side
        for rule in mr.approval_rules.fetch(force=True):
            if rule['name'].startswith('Blocked-by:'):
                for person in rule['eligible_approvers']:
                    if person['id'] == lab.me['id']:
                        return False
        mr.approval_rules.create(name='Blocked-by: {}'.format(lab.me['username']),
                                 approvals_required=1, user_ids=[lab.me['id']])
        mr.new_thread(body='/block', blocking=False)
        return True

    def unblock(self, mr):
        for rule in mr.approval_state['rules']:
            if (rule['name'].startswith('Blocked-by:') and
                    len(rule['eligible_approvers']) == 1 and
                    rule['eligible_approvers'][0]['id'] == lab.me['id']):
                rule_id = rule['id']
                break
        else:
            return False
        try:
            mr.approval_rules.delete(rule_id)
        except gitlab.NotFoundError:
            # concurrent delete from web; that's fine
            return False
        mr.new_thread(body='/unblock', blocking=False)
        return True

    def get_diff(self, sha):
        result = None
        if self.kscope:
            # Try Kerneloscope first
            result = self.kscope.diff(sha)
        if not result:
            # Fetch from upstream cgit. Like with other git hostings, all forks
            # share the same object storage. This allows us to get the commit
            # from any kernel tree hosted on git.kernel.org just by querying the
            # main repo.
            resp = self.kgit.request('GET',
                                    '/pub/scm/linux/kernel/git/torvalds/linux.git/patch/',
                                    fields={'id': sha})
            if resp.status == 200:
                return resp.data.decode('utf-8', errors='replace')
        return result

    def get_fixes(self, commit_list, mr, repo):
        if not self.kscope:
            return ()
        fixes = self.kscope.fixes(commit_list, repo.remote.url, mr['target_branch']) or []
        fixing = self.kscope.fixed_commits(commit_list, repo.remote.url, mr['target_branch'])
        fixes.extend(fixing)
        series = self.kscope.series(commit_list, repo.remote.url, mr['target_branch'])
        fixes.extend((s[0], s[1], s[2], 'series') for s in series)
        return fixes

    def exclude_fixes(self, commits, mr):
        result = [match.group(1) for match in self._omit_re.finditer(mr['description'])]
        for c in commits:
            result.extend(match.group(1) for match in self._omit_re.finditer(c.message))
        return result

    def get_template_names(self):
        return (('fixes', 'list of missing fixes'),)

    def get_template(self, key, commit_list):
        if key != 'fixes':
            return ''
        lines = []
        for commit in commit_list:
            if commit.type != 'suggested':
                continue
            lines.append('{} {}'.format(commit.get_sha(12), commit.name))
        return '\n'.join(lines)

    def _load_owners(self):
        if self.owners:
            return
        try:
            self.owners = kernel_owners.Owners(24)
        except kernel_owners.OwnersError as e:
            raise packages.PackageError(str(e))

    def get_file_owner_names(self, files):
        self._load_owners()
        return self.owners.get_subsystems(files)

    def get_file_owners(self, key):
        self._load_owners()
        return self.owners.get_includes(key), self.owners.get_excludes(key)

    def postformat_comment(self, comment, for_commit):
        if not for_commit:
            return comment
        if not self._nack_re.search(comment):
            return comment
        result = []
        nacks = []
        skip_empty = False
        for line in comment.split('\n'):
            if self._nack_re.match(line):
                nacks.append(line.lstrip())
                skip_empty = True
                continue
            if line.strip() == '' and skip_empty:
                continue
            skip_empty = False
            result.append(line)
        if result and result[-1].strip() != '':
            result.append('')
        result.extend(nacks)
        return '\n'.join(result)

    def filter_note(self, note, new_thread):
        if note['system']:
            if (note['body'].startswith('approved this merge request') or
                note['body'].startswith('unapproved this merge request') or
                note['body'].startswith('marked this merge request as')):
                return True
            return False
        if note['author']['username'] in ('cki-kwf-bot', 'cki-admin-bot'):
            return False
        if (note['author']['username'].startswith('project_') and
            '_bot_' in note['author']['username']):
            # bots with username such as project_24152864_bot_874f2f9520e2e746c535c1ffad26708f
            return False
        if self._bot_re.match(note['author']['username']):
            return False
        if (note['body'].startswith('Passed lnst testing') or
            note['body'].startswith('Skipping lnst testing')):
            return False
        if self._cmd_re.match(note['body'].strip()):
            return False
        return True


packages.add(Kernel)
