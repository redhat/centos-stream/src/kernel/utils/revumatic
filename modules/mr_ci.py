import re
import tuimatic

from modules import gitlab

class MRCIStatus:
    max_retries = 10

    def __init__(self, app, mr):
        self.app = app
        self.mr = mr

        self._repo_re = re.compile(r'Add it to a system: .*(http.*\.repo)', re.MULTILINE)

    def fetch(self):
        self.wait = self.app.wait_popup('Fetching the merge request CI status...')
        self.data = []
        self.cur_text = []
        self.fetch_merge_status()

    def fetch_done(self):
        self.add_widget()
        self.wait.stop()
        self.wait = None
        widget = tuimatic.Filler(tuimatic.Pile([('pack', w) for w, _ in self.data]), valign='top')
        self.popup = self.app.popup(self.callback, popup_widget=widget, height='max')
        for w, name in self.data:
            if name:
                self.popup.register_widget(name, w)
        self.popup.set_default_button(True, True)
        self.reshow()

    def reshow(self, **kwargs):
        self.app.popup_show(self.popup)

    def add_text(self, *args):
        for text in args:
            self.cur_text.append(text)

    def add_widget(self, widget=None, name=None):
        if self.cur_text:
            viewer = tuimatic.Viewer(self.cur_text)
            self.data.append((viewer, None))
            self.cur_text = []
        if widget:
            self.data.append((widget, name))

    def add_list(self, name, items):
        widgets = []
        for item in items:
            w = tuimatic.SelectableIcon('▶ ' + item)
            w.set_wrap_mode('ellipsis')
            widgets.append(('pack', tuimatic.AttrMap(w, 'list', self.app.focus_map)))
        box = tuimatic.Pile(widgets)
        self.add_widget(box, name)

    def fetch_merge_status(self, counter=0):
        mr = self.mr.copy(with_merge_status_recheck=True)
        if mr['merge_status'] in ('unchecked', 'checking'):
            if counter < self.max_retries:
                self.app.loop.set_alarm_in(1, self.fetch_merge_status, counter + 1)
                return
            merge_status = ('desc-warn', '???')
        elif mr['merge_status'] == 'can_be_merged':
            merge_status = ('desc-ok', 'ok')
        elif mr['merge_status'] in ('cannot_be_merged', 'cannot_be_merged_recheck'):
            merge_status = ('desc-err', 'conflict (needs a rebase)')
        else:
            merge_status = ('desc-warn', '???')
        self.add_text('merge: ', merge_status, '\n')
        self.fetch_pipeline()

    def get_job_status(self, status):
        if status == 'success':
            return 1
        elif status == 'skipped':
            return 2
        elif status in ('created', 'waiting_for_resource', 'preparing', 'pending',
                        'running', 'scheduled'):
            return 3
        elif status == 'canceled':
            return 4
        elif status == 'failed':
            return 5
        elif status == '404':
            return 6
        return 0

    def str_status(self, status):
        return [('desc-warn', '???'),
                ('desc-ok', 'ok'), ('desc-warn', 'skipped'),
                ('desc-warn', 'in progress'), ('desc-err', 'canceled'),
                ('desc-err', 'failed'), ('desc-warn', 'inaccessible')][status]

    def fetch_pipeline(self, counter=0):
        status = 0
        self.failed = []
        self.repos = []
        pipeline = self.mr.head_pipeline
        if pipeline is not None:
            try:
                for bridge in pipeline.bridges():
                    if 'realtime' in bridge['name']:
                        continue
                    br_status = self.get_job_status(bridge['status'])
                    if br_status > status:
                        status = br_status
                    for job in bridge.downstream_pipeline.jobs():
                        if job['status'] == 'failed':
                            self.failed.append(('failed {}'.format(job['name']), bridge['name'],
                                                job))
                            continue
                        if job['name'].startswith('publish ') and job['status'] == 'success':
                            arch = job['name'][8:]
                            self.repos.append([arch, bridge['name'], job, None])
            except gitlab.NotFoundError:
                status = self.get_job_status('404')

        self.add_text('testing: ', self.str_status(status), '\n')

        if self.failed:
            self.add_text('\nLogs:')
            self.add_list('log', self.format_with_optional_bridge(self.failed))

        if self.repos:
            self.add_text('\nRepos:')
            self.add_list('repo', self.format_with_optional_bridge(self.repos))

        self.fetch_done()

    def format_with_optional_bridge(self, items):
        """Formats the 'items' to include a bridge name if there's more than
        one bridge. The 'items' has to be a list, each item is a tuple or
        a list with at least two items. The first one is the name to be
        displayed, the second one is the bridge name. If there is only one
        bridge present it 'items', an iterable with just names is returned.
        If there are multiple bridges, an iterable with "name (bridge)" is
        returned."""
        bridges = set(data[1] for data in items)
        if len(bridges) <= 1:
            return (data[0] for data in items)
        return ('{} ({})'.format(data[0], data[1]) for data in items)

    def callback(self, log=None, repo=None):
        if log is not None:
            job = self.failed[log][2]
            data = None
            wait = self.app.wait_popup('Fetching the log...')
            # first, try the build log
            try:
                data = job.artifact('artifacts/build.log')
            except gitlab.NotFoundError:
                pass
            if not data:
                # if not available, load the generic job log
                data = job.trace
            wait.stop()
            self.app.view_popup(self.reshow, data)
            return
        if repo is not None:
            _, _, job, path = self.repos[repo]
            if not path:
                wait = self.app.wait_popup('Fetching the log...')
                match = self._repo_re.search(job.trace)
                wait.stop()
                if match:
                    path = match.group(1)
                    self.repos[repo][2] = path
            if not path:
                self.app.message_popup(self.reshow, 'Repo URL not found.')
            else:
                self.app.view_popup(self.reshow, path)
            return
