import tuimatic

class Package:
    """Abstract class for defining downstream package specific data and
    handling.

    Class attributes:
    paths - an iterable of GitLab path prefixes that belong to this
    package.
    marks - number of package specific marks in the commit list.

    Signals:
    status-changed - indicates that the status was changed, no parameters.
    The status will be queried via get_status."""
    paths = ()
    marks = 0
    config_section = None

    @classmethod
    def register_settings(cls):
        """You may redefine this in your subclass. It should create a custom
        config section and register all config options this class needs."""
        pass

    @classmethod
    def dynamic_init(cls):
        """You may redefine this in your subclass. It is called before any
        object of the class is instantiated but after settings were loaded
        and utils.lab was set. Any class attributes can be modified based on
        the config or fetched from GitLab."""
        pass

    def get_status(self):
        """You may redefine this in your subclass. It will be called to
        obtain the current status as markup text. This will be only called
        if this or other package sent the status-changed signal. If you need
        to return more tuples consisting the markup text, return a list (not
        a tuple) of the tuples. For no status, return None."""
        return None

    def get_extra_help(self):
        """You may redefine this in your subclass. The return value should
        be markup text to use as additional help text. For no extra help,
        return None."""
        return None

    def is_hidden(self, path):
        """You may redefine this in your subclass. It should return True if
        the given GitLab project path is recognized by this package but MRs
        from this project should not appear in lists (TODO list, filtered
        list, etc.). Note that it's still possible to open them by their URL
        or from the 'mine --author' list."""
        return False

    def get_base_sha(self, mr):
        """You may redefine this in your subclass. It's used to override the
        base SHA of a merge request. It should return the base SHA of the
        passed merge request (gitlab.MR object) as a string, or None to use
        the base SHA that was returned by GitLab."""
        return None

    def get_historical_base_sha(self, version, activity):
        """You may redefine this in your sublass. It has a similar purpose
        as get_base_sha but for the given version (which is not the latest
        version). It should return a string or None to use the GitLabs's
        base SHA."""
        return None

    def get_bug_urls(self, mr):
        """You may redefine this in your subclass. Given the merge request
        (gitlab.MR object), it should return a list of bug tracker URLs. It
        can also return None."""
        return None

    def get_upstream(self, commit):
        """You should redefine this in your subclass. It's called to obtain
        upstream commit ids from a downstream commit. The commit parameter
        is a pygit2 Commit object. The return value should be the upstream
        commit SHAs as a list of strings, None if this is a downstream-only
        commit, or an empty list if upstream relation cannot be inferred."""
        return None

    def get_marks(self, commit, diff):
        """You can redefine this in your subclass. It should return an
        iterable with package specific marks. Each mark is a single
        character string; use " " for no mark. The number of marks must be
        equal to the class attribute 'marks'. The commit parameter is
        a pygit2 Commit object, the diff parameter is Revumatic patch.Patch
        object, which was already compared to upstream."""
        return ()

    def get_marks_help(self):
        """You can redefine this in your subclass. It should return a list
        of (mark, description, color_class) tuples. For the color class
        explanation, see get_label_color."""
        return ()

    def get_label_color(self, label):
        """You can redefine this in your subclass. It's used to pick colors
        for labels and to filter out unwanted labels. Given a label name, it
        should return 'ok', 'warn', 'err' or other defined color class (see
        the color classes starting with '*label-' in the main revumatic
        file). For the default color, return an empty string. To skip the
        label, return None."""
        return ''

    def filter_list_labels(self, labels, flags):
        """You can redefine this in your subclass. Given the list of labels,
        it should return a list of those that should be shown in the todo
        and similar lists. The 'flags' parameter contains a list of flags
        that will displayed together with labels; this is useful if you want
        to select different labels based on the MR state, such as 'draft',
        'merged' or 'closed'."""
        return ()

    def get_todo_action(self, raw_todo_data):
        """You can redefine this in your subclass. Given the data returned
        by GitLab for a TODO list entry, return a dict with data to display
        about how the entry got into the TODO list, or None for the default
        processing. The dict can contain the following keys, all of which
        are optional: 'who' -- who caused the entry to be added (a string);
        'action' -- what is the action expected from the user (e.g. "review
        of"); 'body' -- further information about the entry."""
        return None

    def get_blocks(self, mr):
        """You can redefine this in your subclass. Return a list of GitLab
        person dicts (containing at least 'id', 'username' and 'name' keys)
        that rejected the MR.
        Return None (as opposed to an empty list) to indicate that
        a package-specific MR blocking is not supported."""
        return None

    def block(self, mr):
        """You can redefine this in your subclass. It should block the given
        MR from merging.
        Return True if the MR should be re-added to the TODO list.
        This will not be called if get_blocks returned None."""
        return False

    def unblock(self, mr):
        """You can redefine this in your subclass. It should unblock the
        given MR.
        Return True if the MR should be re-added to the TODO list.
        This will not be called if get_blocks returned None."""
        return False

    def get_diff(self, sha):
        """You may redefine this in your subclass. It's called when the
        given commit id could not be found in any of the known repos. Return
        the diff as a string or None. Note that this will be ever called
        only to get upstream commits."""
        return None

    def get_fixes(self, commit_list, mr, repo):
        """You may redefine this in your subclass. It's used to obtain fixes
        for the upstream commits in commit_list. The commit_list is a list
        of commit SHAs; the mr is a gitlab.MR object; the repo is
        a git.ProjectRepo object. The returned value must be an iterable
        with tuples (upstream_commit_sha, fix_commit_sha, fix_subject,
        suggestion_type). It's okay if the returned fixes contain also those
        that are part of the original commit_list; those will be filtered
        out automatically. However, it's up to the Package subclass to
        process the fixes recursively. In other words, upstream_commit_sha
        must always be one of the commits from the initial commit_list. See
        mr_commits.DispCommit.set_type_suggested for the list of recognized
        suggestion_type values."""
        return ()

    def exclude_fixes(self, commits, mr):
        """You may redefine this in your subclass. It is called after
        get_fixes and can request certain fixes to be filtered out, most
        often because the backport author indicated so in the backport. The
        commits is a list of pygit2 Commit objects of the backport; the mr
        is a gitlab.MR object. The returned value must be an iterable of
        commit shas; it's okay to return short shas."""
        return ()

    def get_template_names(self):
        """You may redefine this in your subclass. It should return an
        iterable with tuples (key, name). The returned names are used to
        present a menu when the user presses 'ctrl t'. The corresponding key
        is used as an argument to get_template to get the corresponding
        template to be inserted."""
        return ()

    def get_template(self, key, commit_list):
        """You may redefine this in your subclass. Given the key, it should
        return the corresponding template as a markup (using attributes is
        discouraged, though)."""
        return ''

    def get_file_owner_names(self, files):
        """You may redefine this in your subclass. It should return an
        iterable with tuples (key, name). The returned names are used to
        present a menu when the user presses 'L'. The corresponding key is
        used as an argument to get_file_owners to get the corresponding
        regexes for file filtering. As a special case, if the key is None,
        selecting that item will clear the filter. Markup is permitted in
        names."""
        return ()

    def get_file_owners(self, key):
        """You may redefine this in your subclass. Given the key, it should
        return a tuple of (include_regex, exclude_regex). The regex is
        passed to re.search; if you want it to match at the beginning or end
        of the string, include r'\A' and r'\Z' as appropriate."""
        return '', None

    def postformat_comment(self, comment, for_commit):
        """You may redefine this in your subclass. It gets a formatted
        comment and a bool indicating whether this is a comment for a commit
        and returns a reformatted comment."""
        return comment

    def filter_note(self, note, new_thread):
        """You may redefine this in your subclass. It's called to decide
        whether the given note should be displayed. The note is a dictionary
        with note data (see GitLab API). new_thread is True if this note
        starts a new thread, False if it's a continuation of a thread. The
        method should return True to display the note, False to skip the
        note or raise a PackageSkipThread exception to skip the note and the
        rest of the thread."""
        return True


class PackageError(Exception):
    pass

class PackageSkipThread(Exception):
    pass


class PackageRegistry:
    def __init__(self):
        self.classes = []
        self._inited = False

    def add(self, pkg_class):
        # The added item is [class, instance]. The instance is allocated
        # lazily.
        self.classes.append([pkg_class, None])
        tuimatic.register_signal(pkg_class, 'status-changed')

    def _get(self, pkg):
        if not pkg[1]:
            pkg[1] = pkg[0]()
            # resend signals:
            tuimatic.connect_signal(pkg[1], 'status-changed', tuimatic.emit_signal,
                                    weak_args=[self], user_args=['status-changed'])
        return pkg[1]

    def get(self, name):
        assert self._inited
        for pkg in self.classes:
            if pkg[0].__name__ == name:
                return self._get(pkg)

    def find(self, path, exclude_hidden=False):
        assert self._inited
        for pkg in self.classes:
            if any(path.startswith(p) for p in pkg[0].paths):
                result = self._get(pkg)
                if exclude_hidden and result.is_hidden(path):
                    return None
                return result

    def get_paths(self):
        assert self._inited
        result = []
        for pkg in self.classes:
            result.extend(pkg[0].paths)
        return result

    def register_settings(self):
        assert not self._inited
        for pkg in self.classes:
            pkg[0].register_settings()

    def collect(self, callback):
        """Calls the callback on each of the instantiated packages,
        returning an iterable of callback results. The callback gets
        a single parameter, a Package object."""
        assert self._inited
        return filter(None, (callback(pkg[1]) for pkg in self.classes if pkg[1]))

    def init(self):
        """Must be called after utils.init_config and utils.init_lab."""
        for pkg in self.classes:
            pkg[0].dynamic_init()
        self._inited = True


tuimatic.register_signal(PackageRegistry, 'status-changed')
registry = PackageRegistry()

add = registry.add
find = registry.find
get = registry.get
get_paths = registry.get_paths
