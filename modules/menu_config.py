import copy
import tuimatic

from modules.settings import settings
from modules import (
    keys,
    menu_base,
)

class Screen(menu_base.MenuScreen):
    command = 'config'
    help = 'configure Revumatic'

    def add_actions(self, actions):
        actions.extend((
            { 'name': 'open', 'prio': 2, 'help': 'select' },
            { 'name': 'quit', 'prio': 0, 'help': self.app.get_quit_text(),
              'confirm': 'Do you want to save the changes and exit the configuration?' },
        ))

    def get_title(self):
        return 'Revumatic configuration'

    def load_keys(self):
        sections = [sect for sect in settings if not sect.hidden]
        sections.sort()
        return [sect.name for sect in sections]

    def format_item(self, key):
        return '► {}'.format(settings[key].display_name)

    def open(self):
        section_name = self.current_key
        if section_name is None:
            return
        elif section_name == 'keys':
            self.app.start_screen(KeyScreen)
        else:
            self.app.start_screen(OptionScreen, section_name=section_name)

    def save(self):
        settings.save()

    def action(self, what, widget, size):
        if what == 'quit':
            self.save()
            self.app.quit()
        elif what == 'open':
            self.open()


class OptionScreen(menu_base.MenuScreen):
    def __init__(self, app, args):
        super().__init__(app, args)
        self.section_name = args.section_name

    def add_actions(self, actions):
        actions.extend((
            { 'name': 'open', 'prio': 2, 'help': 'change' },
            { 'name': 'remove', 'prio': 2, 'help': 'unset' },
            { 'name': 'back', 'prio': 0, 'help': 'back' },
        ))

    def get_title(self):
        return settings[self.section_name].display_name

    def load_keys(self):
        options = settings[self.section_name].registered_options()
        self.disp_name_width = max(len(o['display_name']) for o in options
                                                          if o['type'] != 'section')
        return [option['name'] for option in options]

    def format_item(self, key):
        section = settings[self.section_name]
        option = section.options[key]
        if option['type'] == 'section':
            return '► {}'.format(option['display_name'])
        result = []
        result.append('{:{}}  '.format(option['display_name'], self.disp_name_width))
        value = section[key]
        if 'masked' in option and value:
            value = '●●●●●●'
        elif type(value) == list:
            value = ', '.join(str(v) for v in value)
        elif type(value) == bool:
            value = 'yes' if value else 'no'
        elif value:
            value = str(value)
        if key not in section:
            if value:
                result.append(('list-comment', 'not set, default: '))
            else:
                result.append(('list-comment', 'not set'))
        if value:
            result.append(value)
        return result

    def get_help(self, key):
        return settings[self.section_name].options[key]['help_text']

    def restored(self):
        self.update_current_item()

    def store_result(self, option, text=None):
        if text is None:
            return
        if option['type'] == int:
            try:
                value = int(text)
            except ValueError:
                self.app.message_popup(self.input_line, 'Please enter a number.',
                                       callback_args=(option, text))
                return
        elif option['type'] == bool:
            value = text == 'yes'
        else:
            value = text
        settings[self.section_name][option['name']] = value
        self.update_current_item()

    def input_line(self, option, value, button=None):
        self.app.edit_popup(self.store_result, option['display_name'], value, allow_esc=True,
                            callback_args=(option,))

    def input_list(self, option, items, default_text):
        self.app.listbox_popup(self.store_result, items, default_text,
                               callback_args=(option,))

    def open(self):
        key = self.current_key
        if key is None:
            return
        option = settings[self.section_name].options[key]
        value = settings[self.section_name][key]
        if option['type'] in (str, int):
            self.input_line(option, str(value))
        elif option['type'] == bool:
            self.input_list(option, ('yes', 'no'), 'yes' if value else 'no')
        elif type(option['type']) == tuple:
            self.input_list(option, option['type'], value)
        elif type(option['type']) == list:
            self.app.start_screen(ItemsScreen, section_name=self.section_name,
                                  option_name=key)
        elif option['type'] == 'section':
            self.app.start_screen(OptionScreen, section_name=value)
        return

    def remove(self):
        key = self.current_key
        if key is None:
            return
        del settings[self.section_name][key]
        self.update_current_item()

    def action(self, what, widget, size):
        if what == 'back':
            self.app.quit()
        elif what == 'open':
            self.open()
        elif what == 'remove':
            self.remove()


class ItemsScreen(menu_base.MenuScreen):
    def __init__(self, app, args):
        super().__init__(app, args)
        self.section_name = args.section_name
        self.option_name = args.option_name
        # only list of strings is supported currently
        options = settings[self.section_name].options[self.option_name]
        assert(type(options['type']) == list and options['type'][0] == str)

    def add_actions(self, actions):
        actions.extend((
            { 'name': 'add', 'prio': 2, 'help': 'add new' },
            { 'name': 'remove', 'prio': 2, 'help': 'remove' },
            { 'name': 'back', 'prio': 0, 'help': 'back' },
        ))

    def get_title(self):
        return settings[self.section_name].options[self.option_name]['display_name']

    def load_keys(self):
        return settings[self.section_name][self.option_name]

    def store_result(self, text=None):
        if text is None:
            return
        settings[self.section_name].add(self.option_name, text)
        self.add_key(text)

    def add(self):
        self.app.edit_popup(self.store_result, 'New item:', allow_esc=True)

    def remove(self):
        key = self.current_key
        if key is None:
            return
        settings[self.section_name].remove(self.option_name, key)
        self.delete_current_item()

    def action(self, what, widget, size):
        if what == 'back':
            self.app.quit()
        elif what == 'add':
            self.add()
        elif what == 'remove':
            self.remove()


class KeyScreen(menu_base.MenuScreen):
    def init_ui(self):
        self.key_popup = None
        self.key_map = copy.deepcopy(settings.key_map)
        return super().init_ui()

    def add_actions(self, actions):
        actions.extend((
            { 'name': 'open', 'prio': 2, 'help': 'assign a key' },
            { 'name': 'remove', 'prio': 2, 'help': 'unassign keys' },
            { 'name': 'back', 'prio': 0, 'help': 'back' },
        ))

    def get_title(self):
        return 'Key binding'

    def load_keys(self):
        keys = [k for k in self.key_map]
        self.disp_name_width = max(len(k) for k in keys)
        return keys

    def format_item(self, key):
        result = []
        result.append('{:{}}'.format(key.replace('-', ' '), self.disp_name_width))
        cur_keys = self.key_map[key]
        if key not in self.key_map._modified:
            result.append(' not set, default:')
        result.append(' ')
        for i, k in enumerate(cur_keys):
            if i > 0:
                result.append(', ')
            result.append(('help-key', ' {} '.format(keys.display_keys.get(k, k))))
        return result

    def keypress(self, key):
        if not self.key_popup:
            return False
        self.key_popup.stop()
        self.key_popup = None
        self.key_map[self.current_key] = key
        self.update_current_item()
        return True

    def add_key(self):
        self.key_popup = self.app.popup(popup_widget=tuimatic.SelectableIcon('Press a key'))
        self.app.popup_show(self.key_popup)

    def remove_keys(self):
        del self.key_map[self.current_key]
        self.update_current_item()

    def apply(self):
        settings.clear_section('keys')
        for key in self.walker.objects():
            if key not in self.key_map._modified:
                continue
            settings['keys'][key] = (keys.config_keys.get(k, k) for k in self.key_map[key])

    def action(self, what, widget, size):
        if what == 'back':
            self.apply()
            self.app.quit()
        elif what == 'open':
            self.add_key()
        elif what == 'remove':
            self.remove_keys()
