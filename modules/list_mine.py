from modules.utils import lab
from modules import (
    list_base,
    packages,
)

class Screen(list_base.BaseApprovalsScreen):
    command = 'mine'
    help = 'show the list of your merge requests'
    help_epilog = '''The --approver, --assignee, --author and --reviewer options can be
                  combined. If neither is specified, --approver is assumed.'''
    ui_arg = { 'name': 'which', 'type': ('approver', 'assignee', 'author', 'reviewer'),
               'display_name': 'List the merge requests where you are' }

    name = 'my MR'
    full_name = 'the list of your MRs'
    config_section = 'mine'
    config_section_display_name = 'My list view options'
    default_sort = 'reverse mr_created'

    @classmethod
    def add_arguments(cls, parser):
        super().add_arguments(parser)
        parser.add_argument('--approver', action='store_true',
                            help='show the merge requests having you as an approver')
        parser.add_argument('--assignee', action='store_true',
                            help='show the merge requests assigned to you')
        parser.add_argument('--author', action='store_true',
                            help='show the merge requests created by you')
        parser.add_argument('--reviewer', action='store_true',
                            help='show the merge requests having you as a reviewer')
        parser.set_defaults(cls_kwargs=('author', 'reviewer', 'assignee'))

    def __init__(self, app, args):
        super().__init__(app, args)
        self.which = [k for k in ('approver', 'assignee', 'author', 'reviewer')
                        if getattr(args, k, False)]
        additional = getattr(args, 'which', None)
        if additional and additional not in self.which:
            self.which.append(additional)
        if not self.which:
            self.which = ['approver']

    def fetch_list(self):
        result = []
        seen = set()
        for w in self.which:
            parms = { 'scope': 'all', 'state': 'opened' }
            if w == 'approver':
                parms['approver_ids'] = [lab.me['id']]
            else:
                parms[w + '_id'] = lab.me['id']
            data = lab.mrs(**parms)
            for mr in data:
                if mr['id'] in seen:
                    continue
                seen.add(mr['id'])

                path = mr['references']['full'].split('!')[0]
                pkg = packages.find(path, exclude_hidden=(w != 'author'))
                if not pkg:
                    continue

                result.append(self.parse_mr(pkg, mr, mr, path.split('/')[-1]))
        return result
