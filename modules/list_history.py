from modules.utils import lab
from modules import (
    gitlab,
    list_base,
)

class Key:
    pass

class Screen(list_base.BaseScreen):
    command = 'history'
    help = 'show the history of your actions'

    name = 'history'
    full_name = 'the history'
    config_section = 'history'
    config_section_display_name = 'History list view options'
    default_sort = 'reverse action_date'
    available_sort = ['action_date']
    display_date = 'action_date'
    include_time = True

    def fetch_list(self):
        history = self.app.history.load()
        latest = { record['mr']['references']['full']: i
                   for i, record in enumerate(history) if not record.get('minor') }
        # for the MRs that are present only with minor=True, add the latest
        # minor
        i = len(history)
        for record in reversed(history):
            i -= 1
            key = record['mr']['references']['full']
            if key not in latest:
                latest[key] = i
        # fetch the updated_at fields
        lab.multi_mr_update([record['mr'] for record in history], 'updated_at')
        result = []
        for i, record in enumerate(history):
            path = record['mr']['references']['full']
            project_name = path.split('!')[0]
            last_action_date = gitlab.parse_datetime(history[latest[path]]['date'])
            updated = gitlab.parse_datetime(record['mr']['updated_at']) > last_action_date
            mr = self.parse_mr(not record.get('minor') and latest[path] == i,
                               Key(), record['mr'], project_name.split('/')[-1])
            mr['project'] = project_name
            mr['action_date'] = gitlab.parse_datetime(record['date'])
            if updated:
                mr['flags'].append('updated')
            mr['body'] = record['body']
            result.append(mr)
        return result

    def add_todo(self):
        data = self.walker.get_focused_object()
        self._add_todo(lab.project(data['project']).mr(data['mr_id']))
