import datetime
import json
import re
from urllib.parse import quote

from modules import connection

def parse_datetime(s):
    if isinstance(s, datetime.datetime):
        return s
    if s.endswith('Z'):
        s = s[:-1] + '+0000'
    if '.' not in s:
        # GraphQL format
        return datetime.datetime.strptime(s, '%Y-%m-%dT%H:%M:%S%z')
    # REST format
    return datetime.datetime.strptime(s, '%Y-%m-%dT%H:%M:%S.%f%z')

def encode_datetime(date):
    return date.strftime('%Y-%m-%dT%H:%M:%S.%f%z')


class GitlabError(Exception):
    pass

class ConnectionError(GitlabError):
    pass

class ResponseError(GitlabError):
    def __init__(self, status, path, get_fields, msg=None):
        if msg is None:
            msg = str(status)
        self.msg = msg
        self.status = status
        self.path = path
        self.get_fields = get_fields
        msg = '{} ({})'.format(msg, path)
        if get_fields is not None:
            msg = '{} [{}]'.format(msg, str(get_fields))
        super().__init__(msg)

class NotModifiedError(ResponseError):
    pass

class BadRequestError(ResponseError):
    pass

class AuthenticationError(ResponseError):
    pass

class NotFoundError(ResponseError):
    pass

class ConflictError(ResponseError):
    pass


class GitlabConnection:
    endpoints = { 'REST': '/api/v4', 'GraphQL': '/api/graphql' }

    def __init__(self, host, personal_token, port=443, ca_certs=None):
        try:
            self.pool = connection.https_pool(host, port, ca_certs,
                                              headers={'PRIVATE-TOKEN': personal_token})
        except connection.CertError as e:
            raise ConnectionError(str(e)) from None
        self.me = User(self, path='/user')
        # force fetch to verify the connection is working
        self.me.fetch()

    def _request(self, method, path, fields, headers, raw, api):
        if method == 'GET':
            f = self.pool.request_encode_url
            if fields:
                converted = []
                for k, v in fields.items():
                    if isinstance(v, (list, tuple)):
                        k = k + '[]'
                        converted.extend((k, vv) for vv in v)
                    elif isinstance(v, dict):
                        converted.extend(('{}[{}]'.format(k, kk), vv) for kk, vv in v.items())
                    else:
                        converted.append((k, v))
                fields = converted
            kwargs = { 'fields': fields }
        else:
            f = self.pool.urlopen
            kwargs = {
                'headers': { 'Content-Type': 'application/json' },
                'body': json.dumps(fields),
            }
            kwargs['headers'].update(self.pool.headers)
        path = self.endpoints[api] + path
        try:
            resp = f(method, path, preload_content=False, **kwargs)
        except connection.HTTPError as e:
            raise ConnectionError(connection.error_reason(e)) from None
        if resp.status < 200 or resp.status >= 300:
            try:
                data = json.load(resp)
            except json.JSONDecodeError:
                data = {}
            cls = { 304: NotModifiedError,
                    400: BadRequestError,
                    401: AuthenticationError,
                    404: NotFoundError,
                    409: ConflictError,
                  }.get(resp.status, ResponseError)
            raise cls(resp.status, path, fields, data.get('message'))
        if raw:
            data = resp.read()
        else:
            data = json.load(resp)
        if headers:
            return data, resp.headers
        return data

    def get(self, path='', parms=None, headers=False, raw=False, api='REST'):
        return self._request('GET', path, parms, headers, raw, api)

    def post(self, path='', parms=None, headers=False, raw=False, api='REST'):
        return self._request('POST', path, parms, headers, raw, api)

    def put(self, path='', parms=None, headers=False, raw=False, api='REST'):
        return self._request('PUT', path, parms, headers, raw, api)

    def delete(self, path='', parms=None, headers=False, raw=True, api='REST'):
        return self._request('DELETE', path, parms, headers, raw, api)


class GraphQL:
    """A GraphQL query. Automatically de-paginates but only a single
    level; in such case, expects $cursor variable in the query."""

    def __init__(self, conn, query, page_info=None):
        """page_info is a tuple of dict keys leading to the pageInfo field
        (excluding the pageInfo itself)."""
        self.conn = conn
        self.query = query
        self.page_info = page_info

    def execute(self, variables=[]):
        result = None
        parms = {
            'query': self.query,
        }
        if variables or self.page_info:
            parms['variables'] = variables.copy()
        while True:
            data = self.conn.post(parms=parms, api='GraphQL')
            if 'errors' in data:
                msgs = [d['message'] for d in data['errors'] if 'message' in d]
                raise ResponseError(200, 'GraphQL', '; '.join(msgs))
            data = data['data']
            if result is None:
                result = data
            else:
                self._merge(result, data)
            if not self.page_info:
                break
            # next page
            info = data
            for key in self.page_info:
                info = info[key]
            info = info['pageInfo']
            if not info['hasNextPage']:
                break
            parms['variables']['cursor'] = info['endCursor']
        return result

    def _merge(self, dest, src):
        for k, v in src.items():
            if k not in dest:
                dest[k] = v
            elif type(v) == dict:
                self._merge(dest[k], v)
            elif type(v) == list:
                dest[k].extend(v)
            else:
                # ignore repeated string (int, bool, ...) fields; keep the
                # first value
                pass


class List(list):
    """A list of objects. Automatically de-paginates."""

    _link_re = re.compile(r'<([^>]*)>; *rel="next"')
    _path_re = re.compile(r'https://[^/]*/api/v[^/]*(.*)')

    def __init__(self, item_class, conn, provided, path, get_parms=None):
        super().__init__()
        get_parms = {} if get_parms is None else dict(get_parms)
        get_parms['pagination'] = 'keyset'
        get_parms['per_page'] = 100
        while True:
            data_list, headers = conn.get(path, get_parms, headers=True)
            self.extend(item_class(conn, provided, data=data) for data in data_list)
            link = headers.get('Link')
            if not link:
                break
            match = self._link_re.search(link)
            if not match:
                break
            match = self._path_re.match(match.group(1))
            if not match:
                raise ConnectionError('Unknown pagination link {}'.format(link))
            path = match.group(1)
            get_parms = None


class BaseData:
    """A base for objects returned by API calls. The content is lazy
    fetched. Call fetch() to fetch immediatelly.

    _path is used to construct self.path. It can contain {key} or
    {key->key->key} substitions. The substitutions are satisfied from the
    parent's provides, from path_parms and from the object's data, whatever
    is available.

    In the case of {key1->key2->key3} substitions, the keys specify a chain
    of keys to get value from, i.e. self[key1][key2][key3].

    _provides is a dictionary of keys to export to objects to which this one
    is a parent. The dictionary values specify what should be exported; the
    usual {key->key->key} syntax is supported.

    _attrs and _methods allow on demand construction of properties and
    methods. The key is an attribute name, the value is a tuple of (class,
    path, path_parm, path_parm...). The path is provided only for classes
    that do not implement _path (i.e. List and Str classes); for the rest,
    the path must be omitted and it is taken from that class's _path.

    The path can start with a plus sign, in which case the path of the
    current object will be prepended. The path can include {key->key->key}
    parameter substitions.

    path_parms specify what path_parms to pass to the created object. Each
    path_parms is either a parameter name or 'name=value'. For _attrs, only
    the 'name=value' form makes sense. Note that 'name={key->key}' is
    allowed. With no value, the next positional argument passed to the
    created method is assigned to the 'name'.

    keyword args passed to a created method are used as get_parms.

    The class in _attrs and _methods may also be a string with the class
    name. It may also be a list with a class (or with a class name)
    enclosed, in which case a List is fetched."""
    _path = None
    _provides = {}
    _attrs = {}
    _methods = {}

    _parm_re = re.compile(r'\{([^{}]*)\}')

    def __init__(self, conn, provided=None, path_parms=None, get_parms=None,
                 data=None, path=None):
        """parent is another BaseData instance. If data is specified, the
        object is initialized with that data. Otherwise, it is fetched from
        GitLab. In such case, path_parms optionally specifies parameters for
        path substitution and get_parms optionally specifies parameters for
        GET query. Alternatively, a fully construed path may be specified."""
        self.conn = conn
        self.parms = path_parms or {}
        self.get_parms = get_parms
        self.data = data
        self.provided = provided or {}
        self.path = path
        if path is None:
            try:
                self.path = self._construct_path(self._path)
            except KeyError:
                pass

    def fetch(self, force=False):
        """Fetches the data from GitLab. Safe to be called repeatedly; once
        fetched, the data is cached. Can be called with force=True to force
        fetch, even when data is cached. Note that force=True does not
        discard other cached stuff; in particular, if you already accessed
        an attribute whose content depends on the fetched data, that
        attrbiute will stay cached.
        Returns self to allow constructing assignment trains."""
        if self.data is None or force:
            self.data = self.conn.get(self.path, self.get_parms)
        return self

    def copy(self, **kwargs):
        """Returns an unfetched copy of self. Useful for re-fetching data
        from GitLab."""
        if self.path is None:
            return None
        result = type(self)(self.conn, self.provided, path_parms=self.parms, get_parms=kwargs)
        result.path = self.path
        return result

    def save(self, override_method=None):
        if not hasattr(self, 'save_data'):
            return
        f = self.conn.post
        if override_method:
            f = getattr(self.conn, override_method)
        self.update(f(self.path, self.save_data))
        del self.save_data

    def update(self, new_data):
        if self.data is None:
            return
        self.data.update(new_data)

    def __len__(self):
        self.fetch()
        return len(self.data)

    def get(self, key, fetch=False):
        if fetch:
            self.fetch()
        if self.data:
            if type(self.data) == list:
                if type(key) == int:
                    return self.data[key]
            elif key in self.data:
                return self.data[key]
        if key in self.parms:
            return self.parms[key]
        if key in self.provided:
            return self.provided[key]
        raise KeyError(key)

    def __getitem__(self, key):
        return self.get(key, fetch=True)

    def __setitem__(self, key, value):
        if not hasattr(self, 'save_data'):
            self.save_data = {}
        self.save_data[key] = value

    #def __delitem__(self, key):
    #   not implemented

    def __iter__(self):
        self.fetch()
        return iter(self.data)

    def __contains__(self, item):
        self.fetch()
        return item in self.data

    def _substitute(self, s, in_path=False, extra_dict=None):
        """Substitute all {key->key->key}. Returns KeyError if not
        successful. It does not fetch and is safe to use without a fetch."""
        def replace(match):
            parm = self
            for k in match.group(1).split('->'):
                if extra_dict and parm == self and k in extra_dict:
                    parm = extra_dict[k]
                else:
                    parm = parm.get(k)
                if parm is None:
                    raise KeyError("Cannot substitute '{}'".format(s))
            if in_path:
                return quote(str(parm), safe='')
            return str(parm)

        return self._parm_re.sub(replace, s)

    def _construct_path(self, path, extra_dict=None):
        if path is None:
            raise KeyError('Path not set')
        if path.startswith('+'):
            return self.path + self._substitute(path[1:], in_path=True, extra_dict=extra_dict)
        return self._substitute(path, in_path=True, extra_dict=extra_dict)

    def __getattr__(self, key):
        def args_to_path_parms(args):
            if len(var_parms) != len(args):
                raise TypeError('{}() takes {} positional argument(s) but {} were given'
                                .format(key, len(var_parms), len(args)))
            result = fixed_parms.copy()
            for v, a in zip(var_parms, args):
                result[v] = a
            return result

        method = False
        what = self._attrs.get(key)
        if not what:
            what = self._methods.get(key)
            method = True
        if not what:
            raise AttributeError("'{}' object has no attribute '{}'"
                                 .format(type(self).__name__, key))

        cls = what[0]
        is_list = isinstance(cls, list)
        if is_list:
            cls = cls[0]
        if isinstance(cls, str):
            cls = globals()[cls]

        has_path = hasattr(cls, '_path')
        if is_list or not has_path:
            path = what[1]
            what = what[2:]
        else:
            what = what[1:]

        var_parms = []
        fixed_parms = {}
        for p in what:
            pp = p.split('=', maxsplit=1)
            if len(pp) == 1:
                var_parms.append(p)
            else:
                fixed_parms[pp[0]] = self._substitute(pp[1])

        if method and is_list:
            def result(*args, **kwargs):
                return List(cls, self.conn, self.provides,
                            self._construct_path(path, args_to_path_parms(args)), kwargs)
        elif method and has_path:
            def result(*args, **kwargs):
                return cls(self.conn, self.provides, args_to_path_parms(args), kwargs)
        elif method:
            def result(*args, **kwargs):
                return cls(self.conn, self.provides,
                           self._construct_path(path, args_to_path_parms(args)), kwargs)
        elif has_path:
            result = cls(self.conn, self.provides, fixed_parms)
        else:
            path = self._construct_path(path, fixed_parms)
            result = cls(self.conn, self.provides, path)

        # cache the result in self.key
        setattr(self, key, result)
        return result

    def _get_provides(self):
        # let self.parms and self.data override the provided values
        result = { k: self.get(k) for k in self.provided }
        for k, v in self._provides.items():
            result[k] = self._substitute(v)
        return result
    provides = property(_get_provides)


def Str(conn, provided, path=None, get_parms=None, data=None):
    if data is None:
        data = conn.get(path, get_parms, raw=True).decode(errors='replace')
    return data


class Gitlab(BaseData):
    _path = ''
    _methods = {
        'group': ('Group', 'id'),
        'mrs': (['MR'], '/merge_requests'),
        'project': ('Project', 'id'),
        'todos': (['Todo'], '/todos'),
        'user': ('User', 'id'),
    }

    def __init__(self, host, personal_token, port=443, ca_certs=None):
        conn = GitlabConnection(host, personal_token, port=port, ca_certs=ca_certs)
        super().__init__(conn)
        self.me = self.conn.me

    def multi_mr_update(self, mr_list, field_list):
        """Updates the list of MRs (in place) with the current value of the
        fields in field_list. Note that the update is done using GraphQL and
        not all fields have their direct counterpart in GraphQL. Use with
        care."""
        if not mr_list:
            return
        # convert the field names
        if isinstance(field_list, str):
            field_list = [field_list]
        gql_fields = []
        for field in field_list:
            gql_name = ''.join(c.capitalize() if i > 0 else c
                               for i, c in enumerate(field.split('_')))
            gql_fields.append('{}: {}'.format(field, gql_name))
        # get the list of MR ids to query
        mr_ids = list(set(mr['id'] for mr in mr_list))
        # split the list into chunks to prevent API timeouts
        chunk_start = 0
        chunk_length = 100
        result = None
        while chunk_start < len(mr_ids):
            query = ['query {']
            for mr_id in mr_ids[chunk_start:chunk_start+chunk_length]:
                query.append('_{0}: mergeRequest(id: "gid://gitlab/MergeRequest/{0}") {{'
                             .format(mr_id))
                query.extend(gql_fields)
                query.append('}')
            query.append('}')
            q = GraphQL(self.conn, '\n'.join(query))
            data = q.execute()
            if result is None:
                result = data
            else:
                result.extend(data)
            chunk_start += chunk_length
        # update the MRs
        for mr in mr_list:
            mr.update(result['_{}'.format(mr['id'])])

class User(BaseData):
    _path = '/users/{id}'

    def full_user(self):
        if self['public_email']:
            return '{} <{}>'.format(self['name'], self['public_email'])
        return self['name']


class Project(BaseData):
    _path = '/projects/{id}'
    _provides = { 'Project.id': '{id}' }
    _methods = {
        'commit': ('Commit', 'id'),
        'mr': ('MR', 'project_id={id}', 'iid'),
        'mrs': (['MR'], '+/merge_requests'),
    }


class MR(BaseData):
    _path = '/projects/{project_id}/merge_requests/{iid}'
    _provides = { 'Project.id': '{project_id}', 'MR.iid': '{iid}' }
    _attrs = {
        'approvals': ('Approvals',),
        'approval_rules': ('ApprovalRules',),
        'approval_state': ('ApprovalState',),
        'author': (User, 'id={author->id}'),
        'head_pipeline': ('Pipeline', 'id={head_pipeline->id}'),
        'notes': ('Notes',),
    }
    _methods = {
        'discussion': ('Thread', 'id'),
        'discussions': (['Thread'], '+/discussions'),
        'resource_label_events': (['LabelEvent'], '+/resource_label_events'),
    }

    def versions(self):
        """Returns a list of (version, base_commit_sha). Only the real
        changes made by a force push are returned."""
        result = []
        last = None
        for v in reversed(List(MRVersion, self.conn, self.provides, self.path + '/versions')):
            # GitLab includes also a change of the base sha (caused by
            # a dependency being merged) as a new version. Filter those out,
            # we're interested only in the versions where the head sha
            # changed.
            if last is not None and v['head_commit_sha'] == last['head_commit_sha']:
                result[-1][1] = v['base_commit_sha']
                continue
            result.append([v, v['base_commit_sha']])
            last = v
        assert result[-1][0]['head_commit_sha'] == self['diff_refs']['head_sha']
        return result

    def approve(self, sha):
        new_data = self.conn.post(self.path + '/approve', parms={ 'sha': sha })
        if hasattr(self, 'approvals'):
            self.approvals.update(new_data)

    def unapprove(self):
        new_data = self.conn.post(self.path + '/unapprove')
        if hasattr(self, 'approvals'):
            self.approvals.update(new_data)

    def assign(self, assign=True, user_id=None):
        """Assign or unassign (if assign is False) the MR to the given user.
        user_id=None means the current user."""
        if user_id is None:
            user_id = self.conn.me['id']
        # There's no API to assign or unassign. We always have to provide
        # the full list of MR assignees. To minimize race conditions, always
        # refetch the MR before setting the assignees.
        mr = self.copy()
        assignees = set(a['id'] for a in mr['assignees'])
        if assign:
            if user_id in assignees:
                # already there, no action
                return
            assignees.add(user_id)
        else:
            try:
                assignees.remove(user_id)
            except KeyError:
                # was not an assignee, no action
                return
        self['assignee_ids'] = list(assignees)
        self.save('put')

    def todo(self):
        self.conn.post(self.path + '/todo')

    def is_todo(self):
        """Returns whether the MR is on the current user's TODO list. Always
        queries the server."""
        q = GraphQL(self.conn,
                    '''query ($id: MergeRequestID!) {
                       mergeRequest(id: $id) {
                         currentUserTodos(state: pending, first: 1) {
                           nodes {
                             id
                           }}}}''')
        data = q.execute({ 'id': 'gid://gitlab/MergeRequest/{}'.format(self['id']) })
        return bool(data['mergeRequest']['currentUserTodos']['nodes'])

    def _note_id_to_discussion_id(self, note_id):
        # There's no API to get the discussion id from the note id. We need
        # to get all discussions and find the right one. Interestingly,
        # GraphQL can get a discussion id from a note. But it can't query an
        # individual note, only list all notes in an MR. It's still better
        # than using REST API for this. (In case you wonder why we're not
        # just submitting the note via GraphQL: there's no GraphQL API for
        # adding non-blocking notes.)
        q = GraphQL(self.conn,
                    '''query ($project: ID!, $iid: String!, $cursor: String) {
                       project(fullPath: $project) {
                         mergeRequest(iid: $iid) {
                           notes(after: $cursor) {
                             nodes { id discussion { id } }
                             pageInfo { hasNextPage endCursor }
                           }}}}''',
                    page_info=('project', 'mergeRequest', 'notes'))
        data = q.execute({ 'project': str(self['Project.id']), 'iid': str(self['iid']) })
        note_id = str(note_id)
        for note in data['project']['mergeRequest']['notes']['nodes']:
            if note['id'].rsplit('/', maxsplit=1)[-1] == note_id:
                return note['discussion']['id'].rsplit('/', maxsplit=1)[-1]
        raise IndexError('Discussion id not found for note id {}'.format(note_id))

    def new_thread(self, blocking=True, **kwargs):
        # The discussions API cannot add a non-blocking comment. Use the
        # older notes API whenever possible to prevent the need to resolve
        # the thread afterwards, which looks weird in the web UI.
        need_new_api = blocking
        if not need_new_api:
            for key in kwargs.keys():
                if key not in ('body', 'created_at'):
                    need_new_api = True
                    break

        post_resolve = False
        if need_new_api:
            new_data = self.conn.post(self.path + '/discussions', parms=kwargs)
            if not blocking:
                post_resolve = True
        else:
            new_data = self.conn.post(self.path + '/notes', parms=kwargs)
            # Construct thread data out of the returned note data.
            new_data = {
                'id': self._note_id_to_discussion_id(new_data['id']),
                'individual_note': True,
                'notes': [new_data],
            }
        result = Thread(self.conn, self.provides, data=new_data)
        if post_resolve:
            result.resolve()
        return result


class Todo(BaseData):
    _path = '/todos/{id}'

    def mark_as_done(self):
        new_data = self.conn.post(self.path + '/mark_as_done')
        self.update(new_data)


class Group(BaseData):
    _path = '/groups/{id}'
    _methods = {
        'descendant_groups': (['Group'], '+/descendant_groups'),
        'mrs': ([MR], '+/merge_requests'),
    }


class MRVersion(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/versions/{id}'


class Commit(BaseData):
    _path = '/projects/{Project.id}/repository/commits/{id}'


class Thread(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/discussions/{id}'

    def new_note(self, **kwargs):
        new_data = self.conn.post(self.path + '/notes', parms=kwargs)
        if self.data is not None:
            self['notes'].append(new_data)
        return new_data

    def resolve(self, resolved=True):
        new_data = self.conn.put(self.path, parms={'resolved': resolved})
        self.update(new_data)


class Notes(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/notes'


class LabelEvent(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/resource_label_events/{id}'


class Approvals(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/approvals'


class ApprovalState(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/approval_state'


class ApprovalRules(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/approval_rules'

    def create(self, **kwargs):
        new_data = self.conn.post(self.path, parms=kwargs)
        if self.data is not None:
            self.data.append(new_data)
        return new_data

    def delete(self, rule_id):
        self.conn.delete('{}/{}'.format(self.path, rule_id))
        if self.data is not None:
            for i, rule in enumerate(self.data):
                if rule['id'] == rule_id:
                    del self.data[i]
                    return


class Pipeline(BaseData):
    _path = '/projects/{Project.id}/pipelines/{id}'
    _methods = {
        'bridges': (['BridgeJob'], '+/bridges'),
        'jobs': (['Job'], '+/jobs'),
    }


class BridgeJob(BaseData):
    # _path not set, the API cannot query this object on its own
    _attrs = {
        'downstream_pipeline': (Pipeline, 'Project.id={downstream_pipeline->project_id}',
                                'id={downstream_pipeline->id}'),
    }


class Job(BaseData):
    _path = '/projects/{Project.id}/jobs/{id}'
    _provides = { 'Project.id': '{pipeline->project_id}' }
    _attrs = {
        'trace': (Str, '+/trace'),
    }
    _methods = {
        'artifact': (Str, '+/artifacts/*{path}', 'path'),
    }
