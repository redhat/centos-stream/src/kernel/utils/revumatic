import atexit
import configparser
import contextlib
import copy
import fcntl
import os
import random
import tuimatic
from modules import keys, utils


class Lock:
    def __init__(self, file):
        self.file = file

    def unlock(self):
        self.file.close()
        self.file = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.unlock()


class ConfigDir:
    def __init__(self, path):
        self.path = path
        os.makedirs(self.path, exist_ok=True)
        self.fd = os.open(path, os.O_RDONLY)
        self._main_lock()

    def _main_lock(self):
        """Take a read lock and keep it for the life of the app. It is used
        to determine whether other instances of Revumatic (using the
        particular config dir) are running. The last instance is responsible
        for cleaning up the lock files (excepts for the main lock file; that
        one must stay to ensure race free operation)."""
        self._main_lock_fd = self.open_fd('.lock', os.O_CREAT | os.O_RDWR)
        fcntl.lockf(self._main_lock_fd, fcntl.LOCK_SH)
        atexit.register(self._main_unlock)

    def _main_unlock(self):
        # try to promote the lock to a write lock
        try:
            fcntl.lockf(self._main_lock_fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except BlockingIOError:
            return
        for name in os.listdir(self.fd):
            if name.startswith('.lock.'):
                self.remove(name)

    def open_fd(self, path, flags=os.O_RDONLY):
        return os.open(path, flags, mode=0o666, dir_fd=self.fd)

    def open(self, name, mode='r'):
        """Opens and returns a file named 'name' in the config directory."""
        return open(name, mode=mode, opener=self.open_fd)

    def remove(self, name):
        os.remove(name, dir_fd=self.fd)

    def makedirs(self, path):
        # os.makedirs does not support dir_fd; we thus need to implement it
        # ourselves
        assert(path[0] != '/')
        components = path.split('/')
        for i in range(len(components)):
            try:
                os.mkdir('/'.join(components[:i+1]), dir_fd=self.fd)
            except FileExistsError:
                pass

    def removedirs(self, path):
        assert(path[0] != '/')
        components = path.split('/')
        for i in range(len(components), 0, -1):
            try:
                os.rmdir('/'.join(components[:i]), dir_fd=self.fd)
            except (FileNotFoundError, OSError):
                return

    def exists(self, path):
        assert(path[0] != '/')
        try:
            return os.stat(path, dir_fd=self.fd, follow_symlinks=False)
        except FileNotFoundError:
            return None

    @contextlib.contextmanager
    def rewrite(self, name):
        """Returns a context manager returning a file to write to. The file
        'name' is safely overwritten. The proper locking is the responsibility
        of the caller."""
        suffix = ''.join(chr(random.randint(ord('a'), ord('z'))) for i in range(4))
        tmp_name = '{}.new.{}'.format(name, suffix)
        new_file = self.open(tmp_name, 'w')
        yield new_file
        new_file.close()
        os.rename(tmp_name, name, src_dir_fd=self.fd, dst_dir_fd=self.fd)

    def _try_lock(self, name):
        lock_file = self.open('.lock.{}'.format(name), 'w')
        try:
            fcntl.lockf(lock_file.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
            return lock_file, True
        except BlockingIOError:
            return lock_file, False

    def try_lock(self, name):
        """Returns a lock or None."""
        lock_file, result = self._try_lock(name)
        if result:
            return Lock(lock_file)
        lock_file.close()
        return None

    def lock(self, name, blocking_callback=None, blocking_parms=()):
        """Returns a lock. If the call is going to be blocking, the optional
        blocking_callback is called first."""
        lock_file, result = self._try_lock(name)
        if not result:
            if blocking_callback:
                blocking_callback(*blocking_parms)
            fcntl.lockf(lock_file.fileno(), fcntl.LOCK_EX)
        return Lock(lock_file)


class Settings(metaclass=tuimatic.MetaSignals):
    signals = ['key-map-changed']
    defconfig = {
        'global': {
            'gitlab': 'gitlab.com',
        }
    }
    file_name = 'config'

    def __init__(self):
        self._sections = {}
        self._changes = []
        self._keys_changed = False
        self.key_map = keys.Keys(None)

        sect = self.section('global', 'Common options', 0)
        sect.register('gitlab', str, default=self.defconfig['global']['gitlab'],
                      display_name='GitLab server',
                      help_text='''The hostname of the GitLab server to use.''')
        sect.register('token', str, default='', masked=True,
                      display_name='GitLab API token',
                      help_text='''The GitLab API token to use. To create a token, edit your
                                   profile on GitLab, go to 'Access Tokens' and select the
                                   'api' scope.''')
        sect.register('web-browser', str, default='xdg-open',
                      display_name='Web browser command',
                      help_text='''What command to run to open a web browser. The command
                                   gets requested URLs as parameters; more URLs in a single
                                   command are possible.''')
        sect.register('rh_vpn', bool,
                      display_name='Red Hat internal VPN',
                      help_text='''Do you have access to the Red Hat internal VPN? Leave
                                   unset to auto-detect. Set to 'yes' to get a warning when
                                   you're using Revumatic while not connected to the VPN.
                                   Set to 'no' to skip the auto-detection.''')
        sect.register('repos', [str, '\n'], default=[],
                      display_name='Local git repositories',
                      help_text='''The list of local git repositories to use. This is a mix
                                   of both upstream and downstream repositories. Add your
                                   local clones of upstream repositories to this list.
                                   Revumatic will then be able to compare downstream and
                                   upstream commits and show you the differences.''')
        sect.register('silent', [str, ' '], default=[],
                      display_name='Autoconfirm',
                      help_text='''The list of confirmation dialog boxes that should be
                                   autoconfirmed.''')

        sect = self.section('lists', 'Common list options', 1)
        sect.register('delete-direction', ('down', 'up', 'newer', 'older'), default='down',
                      display_name='Move focus after item deletion',
                      help_text='''Where to move the focus when the currently focused item
                                   is deleted. Set to 'down' or 'up' to move always down or
                                   up on the screen. Set to 'newer' or 'older' to move to
                                   a newer or older item according to the current list
                                   order.''')

        sect = self.section('format', 'Formatting options', 2)
        sect.register('tab', str, default='',
                      display_name='Characters to show as tab',
                      help_text='''Specify one or two characters. Every tab is rendered
                                   as the first character followed by a repetition of the
                                   second character. For example, if the value of this option
                                   is ">-", a tab may be displayed as ">---". If the second
                                   character is omitted, a space will be used.''')
        sect.register('trail', str, default='',
                      display_name='Character to show for trailing spaces',
                      help_text='''Specify a single character. Spaces at the end of a line
                                   will be rendered using this character.''')
        sect.register('unicode-fullwidth', bool, default=True,
                      display_name='Display Unicode double width characters',
                      help_text='''Some emoji and Asian characters take two characters
                                   on the screen. However, not all terminals support that
                                   correctly. With this set to 'no', Revumatic will replace
                                   all double width characters with a question mark.''')

        sect = self.section('keys', 'Configure keys', 20)
        sect.register_any([str, '\n'])

    def _get_parser(self):
        parser = configparser.ConfigParser(default_section=None)
        parser.read_dict(self.defconfig)
        try:
            with self.dir.open(self.file_name) as f:
                parser.read_file(f)
        except FileNotFoundError:
            pass
        return parser

    def init(self, config_path):
        self.dir = ConfigDir(config_path)
        self.parser = self._get_parser()

        self.key_map = keys.Keys(self['keys'])
        tuimatic.emit_signal(self, 'key-map-changed')

    @property
    def format_tab(self):
        return self['format']['tab'] + '  '

    @property
    def format_trail(self):
        return self['format']['trail'] + ' '

    def __getitem__(self, section_name):
        if section_name not in self._sections:
            self._sections[section_name] = Section(self, section_name)
        return self._sections[section_name]

    def section(self, section_name, display_name=None, priority=None):
        result = self[section_name]
        result.set_display(display_name, priority)
        return result

    def __iter__(self):
        return iter(self._sections.values())

    def get(self, section, option, fallback=None):
        return self.parser.get(section, option, fallback=fallback)

    def _apply_change(self, parser, change):
        if not parser.has_section(change['section']):
            parser.add_section(change['section'])
        if change['type'] == 'set':
            parser.set(change['section'], change['option'], change['value'])
        elif change['type'] == 'del':
            try:
                del parser[change['section']][change['option']]
            except KeyError:
                pass
        elif change['type'] in ('add', 'remove'):
            values = parser.get(change['section'], change['option'], fallback='')
            values = [s for s in values.split(change['sep']) if s]
            if change['type'] == 'add':
                if change['value'] not in values:
                    values.append(change['value'])
            else:
                try:
                    values.remove(change['value'])
                except ValueError:
                    pass
            parser.set(change['section'], change['option'], change['sep'].join(values))
        elif change['type'] == 'clear_section':
            parser[change['section']] = {}

    def _change(self, **kwargs):
        self._apply_change(self.parser, kwargs)
        self._changes.append(kwargs)
        if kwargs['section'] == 'keys':
            self._keys_changed = True

    def set(self, section, option, value):
        self._change(type='set', section=section, option=option, value=value)

    def delete(self, section, option):
        self._change(type='del', section=section, option=option)

    def add(self, section, option, value, sep='\n'):
        """Add a new item to a list of items, using 'sep' as the separator."""
        self._change(type='add', section=section, option=option, value=value, sep=sep)

    def remove(self, section, option, value, sep='\n'):
        """Remove an item from a list of items, using 'sep' as the separator."""
        self._change(type='remove', section=section, option=option, value=value, sep=sep)

    def clear_section(self, section):
        self._change(type='clear_section', section=section)

    def options(self, section):
        if section not in self.parser:
            return []
        return self.parser.options(section)

    def has_option(self, section, option):
        return self.parser.has_option(section, option)

    def save(self):
        if not self._changes:
            return
        with self.dir.lock(self.file_name):
            # re-read data
            parser = self._get_parser()
            # apply our changes
            for change in self._changes:
                self._apply_change(parser, change)
            # write data
            with self.dir.rewrite(self.file_name) as f:
                parser.write(f)
            # update the current data
            self.parser = parser
            self._changes = []
        if self._keys_changed:
            tuimatic.emit_signal(self, 'key-map-changed')
            self._keys_changed = False


class Section:
    def __init__(self, settings, section_name):
        self.settings = settings
        self.name = section_name
        self.display_name = section_name
        self.priority = 10
        self.hidden = False
        self.options = {}
        self._option_any_type = None

    def set_display(self, display_name=None, priority=None, hidden=None):
        if display_name:
            self.display_name = display_name
        if priority is not None:
            self.priority = priority
        if hidden is not None:
            self.hidden = hidden

    def __eq__(self, other):
        return self.name == other.name

    def __lt__(self, other):
        if self.priority < other.priority:
            return True
        if self.priority > other.priority:
            return False
        if self.display_name < other.display_name:
            return True
        if self.display_name > other.display_name:
            return False
        return self.name < other.name

    def register(self, option_name, option_type, default=None, masked=False,
                 display_name=None, help_text=None):
        """Register the option in this section. Possible types are: str;
        int; bool; tuple of allowed string values; [TYPE, SEP]; "section".
        Where TYPE is a supported non-list type and SEP is the separator,
        e.g. '\n'. If 'option_type' is the string "section", 'default'
        contains the section name."""
        if not display_name:
            display_name = option_name
        if help_text:
            help_text = utils.reflow(help_text)
        item = { 'name': option_name, 'type': option_type, 'default': default,
                 'display_name': display_name, 'help_text': help_text }
        if masked:
            item['masked'] = True
        self.options[option_name] = item

    def register_any(self, option_type):
        """When registered, used for any option name that is not registered
        specifically."""
        self._option_any_type = option_type

    def get_type(self, option_name):
        try:
            item = self.options[option_name]
        except KeyError:
            if self._option_any_type:
                return self._option_any_type
            raise KeyError("Section '{}': option '{}' not registered"
                           .format(self.name, option_name)) from None
        return item['type']

    def _from_string_single(self, option_type, value):
        if type(option_type) == tuple:
            if value in option_type:
                return value
            # TODO: log a warning
            return None
        if option_type == str:
            return value
        if option_type == int:
            try:
                return int(value)
            except ValueError:
                # TODO: log a warning
                return None
        if option_type == bool:
            value = value.lower()
            if value in ('1', 'yes', 'true', 'on'):
                return True
            if value in ('0', 'no', 'false', 'off'):
                return False
            # TODO: log a warning
            return None
        if option_type == 'section':
            raise ValueError('Cannot override a section name in the config file')
        raise TypeError('Invalid type "{}"'.format(repr(option_type)))

    def _from_string(self, option_type, value):
        if type(option_type) == list:
            result = []
            for v in value.split(option_type[1]):
                if not v:
                    continue
                v = self._from_string_single(option_type[0], v)
                if v is None:
                    continue
                result.append(v)
            return result
        return self._from_string_single(option_type, value)

    def _to_string_single(self, option_type, value):
        if option_type == bool:
            return 'yes' if value else 'no'
        if option_type == 'section':
            raise ValueError('Cannot override a section name in the config file')
        return str(value)

    def _to_string(self, option_type, value):
        if type(option_type) == list:
            result = [self._to_string_single(option_type[0], v) for v in value]
            return option_type[1].join(result)
        return self._to_string_single(option_type, value)

    def __contains__(self, option_name):
        return self.settings.has_option(self.name, option_name)

    def __getitem__(self, option_name):
        t = self.get_type(option_name)
        value = self.settings.get(self.name, option_name, fallback=None)
        if value is not None:
            value = self._from_string(t, value)
        if value is None and option_name in self.options:
            value = copy.copy(self.options[option_name]['default'])
        return value

    def __setitem__(self, option_name, value):
        t = self.get_type(option_name)
        value = self._to_string(t, value)
        self.settings.set(self.name, option_name, value)

    def __delitem__(self, option_name):
        self.settings.delete(self.name, option_name)

    def _get_list(self, option_name, value):
        t = self.get_type(option_name)
        if type(t) != list:
            raise TypeError("Section '{}': a list required but option '{}' is a '{}'"
                            .format(self.name, option_name, repr(t)))
        return t

    def add(self, option_name, value):
        t = self._get_list(option_name, value)
        value = self._to_string_single(t[0], value)
        self.settings.add(self.name, option_name, value, sep=t[1])

    def remove(self, option_name, value):
        t = self._get_list(option_name, value)
        value = self._to_string_single(t[0], value)
        self.settings.remove(self.name, option_name, value, sep=t[1])

    def __iter__(self):
        """Returns an iterator over all options present in the config file
        in this section, yielding option_name."""
        return iter(self.settings.options(self.name))

    def items(self):
        """Returns an iterator over all options present in the config file
        in this section, yielding (option_name, option_value)."""
        for option in self.settings.options(self.name):
            yield (option, self[option])

    def registered_options(self):
        """Returns a list of registered option templates, sorted by name."""
        return sorted(self.options.values(), key=lambda o: (o['display_name'], o['name']))


settings = Settings()
