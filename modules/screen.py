class ScreenError(Exception):
    pass


class BaseScreen:
    command = None
    help = None
    help_epilog = None
    help_raw = False
    ui_arg = None

    @classmethod
    def add_arguments(cls, parser):
        return

    @classmethod
    def register_settings(cls):
        return

    def __init__(self, app, args):
        class Widgets:
            pass

        self.widgets = Widgets()
        self.app = app
        self.action_tracker = self.app.new_action_tracker(self.action)

    def start(self):
        widget = self.init_ui()
        self.app.set_workplace(widget)
        self.load_data()

    def init_ui(self):
        return None

    def load_data(self):
        pass

    def get_long_help(self):
        return ()

    def restored(self):
        """Called when a screen that we started exited and we're active again."""
        return

    def action(self, what, widget, size):
        pass

    def keypress(self, key):
        return False
